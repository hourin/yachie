use raylib::prelude::*;
use crate::YachieStatus;

#[derive(PartialEq)]
pub enum CommandOperation {
    MoveTo,
    MoveDir,
    PenUp,
    PenDown,
    RotationRel,
    RotationAbs,
    Hide,
    Show,
    Quit,
}

pub struct Command {
    pub operation: CommandOperation,
    pub operands: [f32; 2],
}

impl Command {
    pub fn from(string: &str) -> Command {
        let string = string.trim().split(' ').collect::<Vec<&str>>();
        let mut command = Command {
            operation: CommandOperation::PenUp,
            operands: [0.0, 0.0]
        };

        match string[0] {
            "u" => {
                println!("PEN UP");
                command.operation = CommandOperation::PenUp;
            }
            "U" => {
                println!("PEN DOWN");
                command.operation = CommandOperation::PenDown;
            }
            "h" => {
                println!("YACHIE HIDDEN");
                command.operation = CommandOperation::Hide;
            }
            "H" => {
                println!("YACHIE ShowN");
                command.operation = CommandOperation::Show;
            }
            "q" | "Q" => {
                println!("BYE WORLD");
                command.operation = CommandOperation::Quit;
            }
            "M" => {
                println!("MOVE TO OP1:OP2");
                command.operation = CommandOperation::MoveTo;
            }
            "m" => {
                println!("MOVE AT FACING DIRECTION WITH OP1 STEPS");
                command.operation = CommandOperation::MoveDir;
            }
            "r" => {
                println!("RELATIVE ROTATION OP1");
                command.operation = CommandOperation::RotationRel;
            }
            "R" => {
                println!("ABSOLUTE ROTATION OP1");
                command.operation = CommandOperation::RotationAbs;
            }
            &_ => {
                println!("INVALID COMMAND");
            }
        }

        if string.len() > 1 {
            command.operands[0] = string[1].parse::<f32>().unwrap();
        }

        if string.len() > 2 {
            command.operands[1] = string[2].parse::<f32>().unwrap();
        }

        command
    }

    pub fn execute(&self, yachie: &mut YachieStatus) {
        match self.operation {
            CommandOperation::PenUp => { yachie.penup = true; }
            CommandOperation::PenDown => { yachie.penup = false; }
            CommandOperation::Hide => { yachie.hide = true; }
            CommandOperation::Show => { yachie.hide = false; }
            CommandOperation::Quit => { /* Will be handle separately */  }
            CommandOperation::MoveTo => unsafe {
                if !yachie.penup {
                    raylib::ffi::DrawLineV(
                        yachie.goto(Vector2{x: self.operands[0], y: self.operands[1]}).into(),
                        yachie.position.into(),
                        Color::RED.into()
                        );
                } else {
                    yachie.goto(Vector2{x: self.operands[0], y: self.operands[1]});
                }
            }
            CommandOperation::MoveDir => unsafe {
                if !yachie.penup {
                    raylib::ffi::DrawLineV(
                        yachie.godir(self.operands[0]).into(),
                        yachie.position.into(),
                        Color::RED.into()
                        );
                } else {
                    yachie.godir(self.operands[0]);
                }
            }
            CommandOperation::RotationRel => { yachie.rotation += self.operands[0]; }
            CommandOperation::RotationAbs => { yachie.rotation = self.operands[0]; }
        }
    }
}
