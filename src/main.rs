use raylib::prelude::*;
use std::io;
use std::thread;
use std::sync::mpsc;
use std::sync::mpsc::TryRecvError;

mod yachie;
use crate::yachie::*;

mod program;
use crate::program::*;

mod command;
use crate::command::*;

fn main() {
    raylib::set_trace_log(TraceLogLevel::LOG_WARNING);
    let (mut rl, thread) = raylib::init()
        .size(900, 900)
        .title("Yachie")
        .build();

    rl.set_target_fps(60);

    let program = YachieProgram {
        canvas: rl.load_render_texture(&thread, 900, 900).unwrap()
    };

    let mut yachie = YachieStatus::create();

    let mut program_is_running: bool = true;
    let (tx, rx) = mpsc::channel();
    let command_thread = thread::spawn(move || {
        while program_is_running {
            let mut command_string = String::new();
            io::stdin()
                .read_line(&mut command_string)
                .expect("Failed to read command");

            let command = Command::from(&command_string);
            if command.operation == CommandOperation::Quit {
                program_is_running = false;
            }
            tx.send(command).unwrap();
        }
    });

    while program_is_running {
        let mut d = rl.begin_drawing(&thread);

        unsafe {
            raylib::ffi::BeginTextureMode(*program.canvas);

            match rx.try_recv() {
                Ok(command) => {
                    command.execute(&mut yachie);
                    if command.operation == CommandOperation::Quit {
                        program_is_running = false;
                    }
                }
                Err(TryRecvError::Disconnected) => { panic!("DISCONNECTED"); }
                Err(TryRecvError::Empty) => {  }
            }
            raylib::ffi::EndTextureMode();
        }

        d.clear_background(Color::RAYWHITE);
        program.draw_canvas(&mut d);
        yachie.draw(&mut d);
    }

    command_thread.join().unwrap();
}
