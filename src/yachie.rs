use raylib::prelude::*;

// Data for the turtle (called Yachie here)
pub struct YachieStatus {
    // Bad OOP practice. Gonna replace this with get/set soon.
    pub position: Vector2,
    pub rotation: f32,
    pub hide: bool,
    pub penup: bool
}

impl YachieStatus {
    pub fn create() -> YachieStatus {
        YachieStatus {
            position: Vector2{x: 450.0, y: 450.0},
            rotation: 90.0,
            hide: false,
            penup: false
        }
    }

    // Should be a triangle, but for now I'm drawing a circle for simplicity
    pub fn draw(&self, d: &mut RaylibDrawHandle) {
        if !self.hide {
            d.draw_circle_v(self.position, 10.0, Color::BLACK);
        }
    }

    // Instantly teleports to a new position, return old position
    pub fn goto(&mut self, new_position: Vector2) -> Vector2 {
        let old_position = self.position;
        self.position = new_position;
        old_position
    }

    pub fn godir(&mut self, steps: f32) -> Vector2 {
        let old_position = self.position;
        let up = Vector2{
            x: (self.rotation * (3.14 / 180.0)).cos(),
            y: (self.rotation * (3.14 / 180.0)).sin()
        };

        self.position.x += steps * up.x;
        self.position.y -= steps * up.y;
        old_position
    }
}
