use raylib::prelude::*;

pub struct YachieProgram {
    pub canvas: RenderTexture2D
}

impl YachieProgram {
    pub fn draw_canvas(&self, d: &mut RaylibDrawHandle) {
        let src_rec = Rectangle{
            x: 0.0,
            y: 0.0,
            width: 900.0,
            height: -900.0,
        };

        let dest_rec = Rectangle{
            x: 0.0,
            y: 0.0,
            width: 900.0,
            height: 900.0,
        };

        d.draw_texture_pro(
            self.canvas.texture(),
            src_rec, 
            dest_rec,
            Vector2{x: 0.0, y: 0.0},
            0.0,
            Color::WHITE
        );
    }
}
