# YACHIE
Yachie is a free and open source implementation of MS Windows' Logo program,
written in Rust and using raylib for the graphics.

## Building instruction
Building Yachie should be straightforward and similar to building any Cargo
packages. Clone the directory, run
```
cargo build --release
```
and it should be built automatically.

## User manual
Yachie (for now), reads user input from terminal and executes the command.
Commands:
- m: move towards facing direction with OP1 steps.
- M: move to OP1:OP2.
- r: rotates OP1 degrees.
- R: set rotation to OP1.
- h: hide yachie (turtle).
- H: show yachie (turtle).
- u: pen up (stop drawing).
- U: pen down (continue drawing).
- q/Q: quit.

## What it is not
Yachie is an implementation of MS Windows Logo. It is not:
- A guide on how to write Rust (I wrote the program to learn Rust).
- A Rust reference (any Rust learners might have a heart attack reading my Rust code).
- A proprietary program.
